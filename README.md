# JSEdit
JSEdit is a program what you can use as wysiwyg editor. Made with JavaScript, CSS and HTML by https://kaikkitietokoneista.net. 

## Demo

We have demo available in url https://kaikkitietokoneista.github.io/jsedit/. Happy testings!

## Installation

Copy index.html file where do you want and put it inside iframe (not needed). Then edit line 67 and change yourul to url which can handle post data.

## Data handling

PHP example:

```php
<?php

$content = $_POST["content"];

$file = fopen("htmlpage.html", "w") or die("No such file! :OOOO");
fwrite($file, $content);

?>
```
